<div align="center">  
  <img alt="Chatbot" src="img/chatbot.png" height="400px" />    
  <h1>HTML | CSS | Javascript | IBM Watson | NLP </h1>

 :rocket: *Chatbot page: this project will build a interface bot for IBM Watson using javascript, HTML5, CSS3*
  </div>

# :robot: Chatbot page
Chatbot page: [Chatbot project](https://fbduartesc.gitlab.io/chat-bot-with-ibm-watson/)

# :construction_worker: How to run

### :computer: Downloading project 

```bash
# Clone repository into your machine
$ git clone https://gitlab.com/fbduartesc/chat-bot-with-ibm-watson.git
```

### 💻 Running project on a web browser

```bash
# Open the file index.html in your browser
$ index.html
```

# :closed_book: License

Released in 2020.

Made with passion by [Fabio Duarte de Souza](https://gitlab.com/fbduartesc) 🚀.
This project is under the [MIT license](https://gitlab.com/fbduartesc/chat-bot-with-ibm-watson/blob/master/LICENSE).